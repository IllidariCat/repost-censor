#!/bin/sh
set -e

syntax=$'Usage: zip-extension.sh platform version'

if [ -z "$1" ]; then
    echo "Platform is missing. See example";
    echo "$syntax";
    exit 1;
fi

if [ "$1" != "firefox" ] && [ "$1" != "chrome" ]; then 
    echo "Invalid platform '$1'. See example";
    echo "$syntax";
    exit 1;
fi

if [ -z "$2" ]; then
    echo "Version source is missing. See example";
    echo "$syntax";
    exit 1;
fi

if [ "$2" != "npm" ] && [ "$2" != "git" ]; then 
    echo "Invalid version source '$2'. See example";
    echo "$syntax";
    exit 1;
fi

platform="$1";

version="";
if [ "$2" = "npm" ]; then 
    version="$(cat $PWD/package.json | jq -r ".version")";
else
    version="$(git rev-parse --short HEAD)";
fi
name="$(cat $PWD/package.json | jq -r ".name")";

zip -r\
    -FS\
    "$PWD/$name-$version-$platform.zip"\
    "$PWD/dist/extension/firefox"