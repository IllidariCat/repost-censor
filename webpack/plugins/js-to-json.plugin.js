const fs = require('fs');
const path = require('path');

class JsToJsonPlugin {
    constructor(options) {
        this.options = options;
    }

    apply(compiler) {
        compiler.hooks.afterEmit.tap('JsToJsonPlugin', (compilation) => {
            const entries = this.options.entries;
            if (!Array.isArray(entries)) {
                throw new Error("Entries list is not an array");
            }

            entries.forEach(e => this.processFile(compiler, this.options, e.from, e.to))
        });
    }

    processFile(compiler, options, from, to) {
        if (!from || !to) {
            throw new Error("Entry must contain from and to paths")
        }

        const fromPath = path.resolve(compiler.options.output.path, from);
        const toPath = path.resolve(compiler.options.output.path, to);

        if (!fs.existsSync(fromPath)) {
            throw new Error(`File ${fromPath} does not exists. Make sure it was compiled and present in webpack output`);
        }

        let data = null;
        try {
            data = require(fromPath);
        } catch (e) {
            throw new Error(`Target file (${fromPath}) require failed. Make sure it is a js module that exports plain object. Inner exception: ${e}`);
        }

        if (typeof data !== "object") {
            throw new Error(`Target file (${fromPath}) default export is not an object`);
        }

        let json = null;
        try {
            json = options.pretty ? JSON.stringify(data, null, 2) : JSON.stringify(data);
        } catch (e) {
            throw new Error(`Target file (${fromPath}) cannot be serialized to JSON. Inner exception: ${e}`);
        }

        fs.writeFileSync(toPath, json);

        if (options.removeJs) {
            fs.rmSync(fromPath)
        }
    }
}

module.exports = JsToJsonPlugin;