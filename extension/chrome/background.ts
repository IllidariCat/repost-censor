import { PageActionController } from "../../shared/background-script/show-page-action";
import { ChromeBackgroundScriptClient } from "../../shared/facade/messaging/chrome-messaging.facade";
import { ChromePageActionService } from "../../shared/facade/page-action/chrome-page-action.facade";
import { ChromeSyncStorage, ChromeSessionStorage } from "../../shared/facade/storage/chrome-storage.facade";
import { ChromeTabs } from "../../shared/facade/tabs/chrome-tabs.facade";

const backgroundClient = new ChromeBackgroundScriptClient();
const pageActionSerivice = new ChromePageActionService();
const syncStorage = new ChromeSyncStorage();
const sessionStorage = new ChromeSessionStorage();
const tabs = new ChromeTabs();

const pageActionController = new PageActionController(
    backgroundClient, 
    pageActionSerivice,
    syncStorage,
    sessionStorage,
    tabs);

pageActionController.start();