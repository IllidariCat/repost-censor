module.exports = {
  description: "A browser extension to censor reposts on social media",
  manifest_version: 3,
  name: "repost-censor",
  version: "1.0.0",
  homepage_url: "https://gitlab.com/IllidariCat/repost-censor",

  icons: {
    16: require("../../shared/assets/icons/No-16.png"),
    32: require("../../shared/assets/icons/No-32.png"),
    48: require("../../shared/assets/icons/No-48.png"),
    128: require("../../shared/assets/icons/No-128.png")
  },

  permissions: [
    "activeTab",
    "storage"
  ],

  background: {
    scripts: ["background.js"]
  },

  page_action: {
    default_icon: {
      19: require("../../shared/assets/icons/No-16.png"),
      38: require("../../shared/assets/icons/No-48.png")
    },
    default_title: "repost_censor",
    default_popup: "popup.html"
  },

  browser_specific_settings: {
    gecko: {
      id: "repost_censor@example.com",
      strict_min_version: "42.0"
    }
  },

  options_ui: {
    page: "options.html",
    open_in_tab: true
  },

  content_scripts: [
    {
      matches: ["*://*.vk.com/*"],
      js: ["warn_vk.js"]
    },
    {
      matches: ["*://*.twitter.com/*"],
      js: ["warn_twitter.js"]
    }
  ]
}