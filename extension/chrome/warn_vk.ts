import { warnVk } from "~shared/content_script/warn_vk";
import { ChromeContentScriptClient } from "~shared/facade/messaging/chrome-messaging.facade";

const chromeClient = new ChromeContentScriptClient();
warnVk(chromeClient);