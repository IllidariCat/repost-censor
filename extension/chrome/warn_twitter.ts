import { warnTwitter } from "~shared/content_script/warn_twitter";
import { ChromeContentScriptClient } from "~shared/facade/messaging/chrome-messaging.facade";

const chromeClient = new ChromeContentScriptClient();
warnTwitter(chromeClient);