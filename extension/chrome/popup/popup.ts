import { ChromeSyncStorage, ChromeSessionStorage } from "../../../shared/facade/storage/chrome-storage.facade";
import { start } from "../../../shared/popup-script/show-author";
import { AuthorsService } from "../../../shared/utils/authors-service";
import { LocalAuthorsList } from "../../../shared/utils/local-authors-list";
import { StaticAuthorsList } from "../../../shared/utils/static-authors-list";

import "./popup.scss";

const syncStorage = new ChromeSyncStorage();
const sessionStorage = new ChromeSessionStorage();
const localList = new LocalAuthorsList(syncStorage);
const staticList = new StaticAuthorsList();

const authorsService = new AuthorsService(localList, staticList)
start(sessionStorage, authorsService);