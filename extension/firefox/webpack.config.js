const path = require('path');

const JsonExportPlugin = require('../../webpack/plugins/js-to-json.plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    entry: {
        background: path.resolve(__dirname, "./background.ts"),
        warn_vk: path.resolve(__dirname, "./warn_vk.ts"),
        warn_twitter: path.resolve(__dirname, "./warn_twitter.ts"),
        manifest: {
            import: path.resolve(__dirname, "./manifest.js"),
            library: {
                type: "commonjs2"
            }
        },
        popup: path.resolve(__dirname, "./popup/popup.ts"),
    },
    module: {
      rules: [
        //Process typescpript files with ts-loader
        {
            test: /\.ts$/,
            loader: 'ts-loader',
            exclude: /node_modules/,
        },
        //Compile manifest to be further processed by the plugin
        {
            test: /manifest\.js$/,
            use: "babel-loader",
        },
        //Load css files as strings
        {
            test: /\.warn\.css$/,
            type: 'asset/source',
        },
        //Load icons
        {
            test: /\.png$/,
            type: 'asset/resource',
            generator: {
                filename: 'icons/[name][ext]'
            }
        },
        //Html files
        {
            test: /\.html$/,
            use: ["html-loader"],
        },
        //Scss
        {
            test: /\.s[ac]ss$/i,
            use: [
                MiniCssExtractPlugin.loader,
                "css-loader",
                "sass-loader",
            ],
        },
      ],
    },
    resolve: {
        //Same as path in tsconfig.json
        alias: {
            '~shared': path.resolve(__dirname, '../../shared'),
        },
        extensions: ['.ts', '.json', '.css', '.scss', '.html'],
    },
    plugins: [
        new JsonExportPlugin({
            entries: [{
                from: "manifest.js",
                to: "manifest.json"
            }],
            pretty: true,
            removeJs: true
        }),
        new MiniCssExtractPlugin(),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, "./popup/popup.html"),
            filename: "popup.html",
            chunks: ['popup'],
        }),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, "./options/options.html"),
            filename: "options.html",
            chunks: [],
        }),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, "./options/feedback.html"),
            filename: "feedback.html",
            chunks: [],
        })
    ],
    output: {
        publicPath: "",
        filename: '[name].js',
        path: path.resolve(__dirname, '../../dist/extension/firefox/'),
        assetModuleFilename: "[name][ext]",
        clean: true
    },
};