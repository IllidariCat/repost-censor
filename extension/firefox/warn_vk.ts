import { warnVk } from "~shared/content_script/warn_vk";
import { FirefoxContentScriptClient } from "~shared/facade/messaging/firefox-messaging.facade";

const firefoxClient = new FirefoxContentScriptClient();
warnVk(firefoxClient);