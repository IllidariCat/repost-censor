import { PageActionController } from "../../shared/background-script/show-page-action";
import { FirefoxBackgroundScriptClient } from "../../shared/facade/messaging/firefox-messaging.facade";
import { FirefoxPageActionService } from "../../shared/facade/page-action/firefox-page-action.facade";
import { FirefoxSyncStorage, FirefoxSessionStorage } from "../../shared/facade/storage/firefox-storage.facade";
import { FirefoxTabs } from "../../shared/facade/tabs/firefox-tabs.facade";

const backgroundClient = new FirefoxBackgroundScriptClient();
const pageActionSerivice = new FirefoxPageActionService();
const syncStorage = new FirefoxSyncStorage();
const sessionStorage = new FirefoxSessionStorage();
const tabs = new FirefoxTabs();

const pageActionController = new PageActionController(
    backgroundClient, 
    pageActionSerivice,
    syncStorage,
    sessionStorage,
    tabs);

pageActionController.start();