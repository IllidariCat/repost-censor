import { warnTwitter } from "~shared/content_script/warn_twitter";
import { FirefoxContentScriptClient } from "~shared/facade/messaging/firefox-messaging.facade";

const firefoxClient = new FirefoxContentScriptClient();
warnTwitter(firefoxClient);