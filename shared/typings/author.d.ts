type Platform = "twitter" | "vk";



type Author = {
    id: string,
    name: string,
    type: Platform
}

type AuthorStatus = {
    inStatic: boolean;
    allowed: boolean;
}

type Authors = {
    [platform in Platform]: Author[];
};