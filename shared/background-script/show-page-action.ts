import { BackgroundScriptMessagingClient } from "../../shared/communication/background.client";
import { ContentScriptType, Events } from "../../shared/communication/messaing";
import { AUTHOR_KEY } from "../constants/background";
import { BackgroundScriptClientFacade, PortClientFacade } from "../facade/messaging/messaging";
import { PageActionFacade } from "../facade/page-action/page-action";
import { SyncStorageFacade, SessionStorageFacade } from "../facade/storage/storage";
import { TabsFacade } from "../facade/tabs/tabs";
import { AuthorsService } from "../utils/authors-service";
import { LocalAuthorsList } from "../utils/local-authors-list";
import { StaticAuthorsList } from "../utils/static-authors-list";

type ContentScript = {
    port: PortClientFacade,
    tabId: number,
    client: BackgroundScriptMessagingClient,
    type?: ContentScriptType,
    author?: Author,
}

export class PageActionController {
    private contentScripts: Record<number, ContentScript> = {};

    private readonly localAuthorsList: LocalAuthorsList;
    private readonly staticAuthorsList: StaticAuthorsList;
    private readonly authorsService: AuthorsService;

    constructor(
        private readonly messagingClient: BackgroundScriptClientFacade,
        private readonly pageActionSerivice: PageActionFacade,
        private readonly syncStorage: SyncStorageFacade,
        private readonly sessionStorage: SessionStorageFacade,
        private readonly tabs: TabsFacade,
    ) {
        this.localAuthorsList = new LocalAuthorsList(this.syncStorage);
        this.staticAuthorsList = new StaticAuthorsList();

        this.authorsService = new AuthorsService(this.localAuthorsList, this.staticAuthorsList);
    }

    public start() {
        this.messagingClient.onConnect(port => this.onConnected(port));
        this.tabs.onActivated(tabId => this.onTabActivated(tabId));
    }

    private onConnected(port: PortClientFacade) {
        const tabId = port.tabId;
        if (tabId === undefined) {
            console.warn("Port has no tab id", port);
            return;
        }

        const client = new BackgroundScriptMessagingClient(port);

        const contentScript: ContentScript = {
            port,
            tabId,
            client,
        }

        this.contentScripts[tabId] = contentScript;

        client.handle<Events.ContentScript.Init>("cs.init", (type) => {
            console.debug(`Content script initialized with type: ${type}`);
            contentScript.type = type;

            return this.authorsService.getBlockedAuthorsOnPlatform(type);
        });

        client.on<Events.ContentScript.AuthorChanged>("cs.authorChanged", async (author) => {
            if (author === null) {
                console.debug(`Gone from author's page. Previous author: ${contentScript.author?.id}`);

                contentScript.author = undefined;
                await this.sessionStorage.remove(AUTHOR_KEY);
                return await this.pageActionSerivice.hide(tabId);
            }

            console.debug(`Author changed from ${contentScript.author?.id} to: ${author.id}`, author);

            contentScript.author = author;
            await this.sessionStorage.set(AUTHOR_KEY, author);
            return await this.pageActionSerivice.show(tabId);        
        });

        port.onDisconnect(() => {
            console.debug(`Disconnected from content script on tab ${tabId}`);
            delete this.contentScripts[tabId];
        });       
        
        console.debug(`Connected to content script on tab ${tabId}`);
    }

    private async onTabActivated(tabId: number) {
        const contentScript = this.contentScripts[tabId];
        if (!contentScript?.author) {
            return;
        }

        await this.sessionStorage.set(AUTHOR_KEY, contentScript.author);
        return await this.pageActionSerivice.show(tabId);        
    }
}