import { ContentScriptMessagingClient } from '../communication/content-script.client';
import { Events } from '../communication/messaing';
import { ContentScriptClientFacade } from '../facade/messaging/messaging';

const POST_ELEMENT_CLASS = "post";
const POST_INSERTION_POINT = "like_btns";
const POST_AUTHOR_ATTRIBUTE = "data-post-author-id";
const WARNING_CLASS = "__repost_warning";
const WARNING_STYLE = "background-color: var(--vkui--color_background_negative_tint);";
const WARNING_TEXT = "Не репость!";

const getPosts = (): HTMLCollectionOf<Element> => 
    document.getElementsByClassName(POST_ELEMENT_CLASS);

const getAuthor = (post: Element): string | null => 
    post.getAttribute(POST_AUTHOR_ATTRIBUTE);

const createWarningElement = () => {
    const element = document.createElement('div');
    element.classList.add("PostBottomAction", "PostBottomAction--withBg", WARNING_CLASS);
    element.setAttribute("style", WARNING_STYLE);
    element.innerText = WARNING_TEXT;
    return element;
}

const processPosts = (authors: Record<string, Author>) => {
    const posts = getPosts();
    for (let i = 0; i < posts.length; i++) {
        const post = posts[i];

        const authorId = getAuthor(post);
        if (authorId === null || !(authors)[authorId]) {
            continue;
        }

        const insertionPoint = post.getElementsByClassName(POST_INSERTION_POINT)[0];
        if (!insertionPoint) {
            continue;
        }

        if (insertionPoint.getElementsByClassName(WARNING_CLASS).length > 0) {
            continue;
        }
        
        const warningElement = createWarningElement();
        insertionPoint.appendChild(warningElement);
    }
}

/** -- Feedback logic */
const URL_META_ELEMENT_SELECTOR = "meta[property='og:url']";
const TITLE_META_ELEMENT_SELECTOR = "meta[property='og:title']";

const pageTypes = [ "public", "event", "id" ] as const;

type AuthorContainer = {
    author: Author | null
}

const isAuthorsPage = (): boolean => {
    const url = document.querySelectorAll(URL_META_ELEMENT_SELECTOR)[0]
        ?.getAttribute("content")
        ?.split("/")
        ?.pop();

    if (!url) {
        console.debug(`Meta tag with url '${URL_META_ELEMENT_SELECTOR}' not found`);
        return false;
    }

    return pageTypes.some(type => url.startsWith(type));
}

const getPageAuthor = (): Author | null => {
    const url = document.querySelectorAll(URL_META_ELEMENT_SELECTOR)[0]
        ?.getAttribute("content")
        ?.split("/")
        ?.pop();

    if (!url) {
        console.debug(`Meta tag with url '${URL_META_ELEMENT_SELECTOR}' not found`);
        return null;
    }

    const type = pageTypes.find(type => url.startsWith(type));
    if (!type) {
        console.debug("Unknown page type", url);
        return null;
    };

    const id = url.split(type).pop();

    if (!id) {
        console.debug("Author id not found, url: ", url);
        return null;
    }

    const name = document.querySelectorAll(TITLE_META_ELEMENT_SELECTOR)[0]
        ?.getAttribute("content");

    if (!name) {
        console.debug(`Meta tag with title '${TITLE_META_ELEMENT_SELECTOR}' not found`);
        return null;
    }

    return {
        id: "-" + id,
        name,
        type: "vk"
    }
}

const checkAuthorPage = (client: ContentScriptMessagingClient, authorContainer: AuthorContainer) => {
    if (isAuthorsPage()) {
        const author = getPageAuthor();
        if (author && authorContainer.author?.id !== author.id) {
            client.emit<Events.ContentScript.AuthorChanged>("cs.authorChanged", author);
        }
        authorContainer.author = author;
    } else if (authorContainer.author !== null) {
        client.emit<Events.ContentScript.AuthorChanged>("cs.authorChanged", null);
        authorContainer.author = null;
    }
}


export const warnVk = async (facade: ContentScriptClientFacade) => {
    const backgroundScriptClient = new ContentScriptMessagingClient(facade, "warn_twitter");
    const authors = await backgroundScriptClient.invoke<Events.ContentScript.Init>("cs.init", "twitter");

    console.debug(`Init complete, received ${authors.length} authors`);

    const lastAuthor: AuthorContainer = { author: null };

    const observer = new MutationObserver((mutationsList, _) => {
        if (mutationsList.some(m => m.type === 'childList')) {
            checkAuthorPage(backgroundScriptClient, lastAuthor);
            processPosts(authors.reduce((acc, a) => ({ ...acc, [a.id]: a }), {}));
        }
    });

    observer.observe(document.body, { childList: true, subtree: true });
}