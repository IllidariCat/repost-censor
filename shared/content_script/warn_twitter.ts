import { ContentScriptMessagingClient } from "../communication/content-script.client";
import { Events } from "../communication/messaing";
import AUTHORS_MAP from "../config/twitter.json";
import { ContentScriptClientFacade } from "../facade/messaging/messaging";
import CSS_STYLES from "../styles/css/twitter.warn.css";

/** -- Additional styles */
const STYLES_CLASS = "__repost_warning_styles"

const addStyles = () => {
    /*
    https://bugzilla.mozilla.org/show_bug.cgi?id=1827104
    const styleSheet = new CSSStyleSheet();
    styleSheet.replaceSync(CSS_STYLES);
    document.adoptedStyleSheets = [stylesheet]
    */

    if (document.head.getElementsByClassName(STYLES_CLASS).length !== 0) {
        return
    }

    const styles = document.createElement("style");
    styles.classList.add(STYLES_CLASS)
    styles.innerHTML = CSS_STYLES;
    document.head.appendChild(styles);
}

/** -- Posts logic */

const POST_ELEMENT_TAG = "article";
const POST_AUTHOR_SELECTOR = "[data-testid='User-Name'] > div > div > a";
const POST_RETWEET_SELECTOR = "[data-testid='retweet']";

const WARNING_CLASS = "__repost_warning";

const getPosts = (): HTMLCollectionOf<Element> => 
    document.getElementsByTagName(POST_ELEMENT_TAG);

const getPostAuthor = (post: Element): string | null => 
    post.querySelectorAll(POST_AUTHOR_SELECTOR)[0]?.getAttribute("href")?.split("/")[1]?.toLowerCase() ?? null;

const checkPostProcessed = (post: Element): boolean => {
    return (post.parentNode as Element)?.classList.contains(WARNING_CLASS);
}

const processPosts = (authors: Record<string, Author>) => {
    const posts = getPosts();
    for (let i = 0; i < posts.length; i++) {
        const post = posts[i];

        const authorId = getPostAuthor(post);
        if (authorId === null || !(authors)[authorId]) {
            continue;
        }

        if (checkPostProcessed(post)) {
            continue;
        }

        (post.parentNode as Element)?.classList.add(WARNING_CLASS);

        const retweet = post.querySelectorAll(POST_RETWEET_SELECTOR)[0];
        if (!retweet) {
            continue;    
        }
    }
}

/** -- Feedback logic */

type AuthorContainer = {
    author: Author | null
}

//User description looks like to be the only thing that has a unique selector on the page
const AUTHOR_SELECTOR = "[data-testid='UserName']";

const isAuthorsPage = (): boolean => {
    return document.querySelectorAll(AUTHOR_SELECTOR).length > 0;
}

const getPageAuthor = (): Author | null => {
    const authorElement = document.querySelectorAll(AUTHOR_SELECTOR)[0];
    if (!authorElement) {
        return null;
    }

    const text = (authorElement as HTMLDivElement).innerText;
    const authorParts = text.split("\n");

    if (authorParts.length < 2) {
        return null;
    }

    const name = authorParts[0];
    const id = window.location.pathname.split("/")[1]?.toLowerCase();

    return {
        id,
        name,
        type: "twitter"
    }
}


const checkAuthorPage = (client: ContentScriptMessagingClient, authorContainer: AuthorContainer) => {
    if (isAuthorsPage()) {
        const author = getPageAuthor();
        if (author && authorContainer.author?.id !== author.id) {
            client.emit<Events.ContentScript.AuthorChanged>("cs.authorChanged", author);
        }
        authorContainer.author = author;
    } else if (authorContainer.author !== null) {
        client.emit<Events.ContentScript.AuthorChanged>("cs.authorChanged", null);
        authorContainer.author = null;
    }
}

export const warnTwitter = async (facade: ContentScriptClientFacade) => {
    const backgroundScriptClient = new ContentScriptMessagingClient(facade, "warn_twitter");
    const authors = await backgroundScriptClient.invoke<Events.ContentScript.Init>("cs.init", "twitter");

    console.debug(`Init complete, received ${authors.length} authors`, authors);

    addStyles();

    const lastAuthor: AuthorContainer = { author: null };

    const observer = new MutationObserver((mutationsList, _) => {
        if (mutationsList.some(m => m.type === 'childList')) {
            checkAuthorPage(backgroundScriptClient, lastAuthor);
            processPosts(authors.reduce((acc, a) => ({ ...acc, [a.id]: a }), {}));
        }
    });

    observer.observe(document.body, { childList: true, subtree: true });
}