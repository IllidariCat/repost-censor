import { PortClientFacade } from "../facade/messaging/messaging";
import { BackgroundMessagingClient, CommandEvent, Handler, MessagingEvent } from "./messaing";

export class BackgroundScriptMessagingClient implements BackgroundMessagingClient {
    private readonly events = new EventTarget();
    private readonly commands = new EventTarget();

    private commandId = 0;

    constructor(private readonly facade: PortClientFacade) {
        //TODO persistent connection?
        this.facade.onMessage(message => this.handleMessage(message));
    }

    on<TEvent extends Handler<string, (...args: any[]) => void>>(
        event: TEvent["event"], 
        fn: TEvent["fn"]
    ): void {
        this.events.addEventListener(event, (message) => {
            const payload = (message as CustomEvent<Parameters<TEvent['fn']>>).detail;
            fn(...payload);
        })
    }

    once<TEvent extends Handler<string, (...args: any[]) => void>>(
        event: TEvent["event"], 
        fn: TEvent["fn"]
    ): void {
        this.events.addEventListener(event, (message) => {
            const payload = (message as CustomEvent<Parameters<TEvent['fn']>>).detail;
            fn(...payload);
        }, { once: true })
    }

    off<TEvent extends Handler<string, (...args: any[]) => void>>(
        event: TEvent["event"], 
        fn: TEvent["fn"]
    ): void {
        this.events.removeEventListener(event, fn);
    }

    emit<TEvent extends Handler<string, (...args: any[]) => void>>(
        event: TEvent["event"], 
        ...args: Parameters<TEvent["fn"]>
    ): void {
        this.facade.postMessage({ type: "event", event, payload: args } as MessagingEvent)
    }

    handle<TEvent extends Handler<string, (...args: any[]) => any>>(
        event: TEvent["event"], 
        handler: TEvent["fn"],
    ): void {
        this.commands.addEventListener(event, async (payload) => {
            const message = (payload as CustomEvent<CommandEvent>).detail;
            try {
                const data = await handler(...message.payload);
                this.facade.postMessage({
                    type: "command",
                    event,
                    id: message.id,
                    payload: data,
                } as CommandEvent);
            } catch (error) {
                this.facade.postMessage({
                    type: "command",
                    event,
                    id: message.id,
                    error: (error as Error).toString(),
                    payload: [],
                } as CommandEvent);
            }
        })
    }

    private handleMessage(message: object): void {
        const messagedEvent = message as MessagingEvent;

        if (messagedEvent.type !== "event" && messagedEvent.type !== "command") {
            return console.warn("Unknown message type received from backgroud script. Skip", message);
        }

        if (!messagedEvent.event) {
            return console.warn("Unknown message type received from backgroud script. Skip", message);
        }

        if (messagedEvent.type === "command") {
            this.commands.dispatchEvent(new CustomEvent(
                messagedEvent.event, 
                { detail: messagedEvent }));
            return;
        }

        this.events.dispatchEvent(new CustomEvent(messagedEvent.event, { detail: messagedEvent.payload }))
    }
}