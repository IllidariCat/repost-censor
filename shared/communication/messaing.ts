export interface ContentScriptMessagingClient {
    on<TEvent extends Handler<string, (...args: any[]) => void>>(
        event: TEvent['event'],
        fn: TEvent['fn'],
    ): void

    once<TEvent extends Handler<string, (...args: any[]) => void>>(
        event: TEvent['event'],
        fn: TEvent['fn'],
    ): void

    off<TEvent extends Handler<string, (...args: any[]) => void>>(
        event: TEvent['event'],
        fn: TEvent['fn'],
    ): void

    emit<TEvent extends Handler<string, (...args: any[]) => void>>(
        event: TEvent['event'],
        ...args: Parameters<TEvent['fn']>
    ): void

    invoke<TEvent extends Handler<string, (...args: any[]) => any>>(
        event: TEvent['event'],
        ...args: Parameters<TEvent['fn']>
    ): Promise<ReturnType<TEvent['fn']>>
}

export interface BackgroundMessagingClient {
    on<TEvent extends Handler<string, (...args: any[]) => void>>(
        event: TEvent['event'],
        fn: TEvent['fn'],
    ): void

    once<TEvent extends Handler<string, (...args: any[]) => void>>(
        event: TEvent['event'],
        fn: TEvent['fn'],
    ): void

    off<TEvent extends Handler<string, (...args: any[]) => void>>(
        event: TEvent['event'],
        fn: TEvent['fn'],
    ): void

    emit<TEvent extends Handler<string, (...args: any[]) => void>>(
        event: TEvent['event'],
        ...args: Parameters<TEvent['fn']>
    ): void

    handle<TEvent extends Handler<string, (...args: any[]) => any>>(
        event: TEvent['event'],
        fn: TEvent['fn'],
    ): void
}

export const ContentScriptType = {
    twitter: "twitter",
    vk: "vk",
} as const;

export type Handler<TEvent extends string, TFn extends (...args: any[]) => void> = {
    event: TEvent,
    fn: TFn
}

export type ContentScriptType = typeof ContentScriptType[keyof typeof ContentScriptType];

export type MessageingEventType = "event" | "command";

export type MessagingEvent = {
    type: MessageingEventType,
    event: string,
    payload: any[]
}

export type CommandEvent = MessagingEvent &{
    type: "command",
    error?: string,
    id: number,
    payload: any,
}

export namespace Events {
    export namespace ContentScript {
        export type Init = Handler<"cs.init", (type: ContentScriptType) => Promise<Author[]>>;
        export type AuthorChanged = Handler<"cs.authorChanged", (author: Author | null) => void>;
    }
}