import { ContentScriptClientFacade, PortClientFacade } from "../facade/messaging/messaging";
import { CommandEvent, Handler, MessagingEvent } from "./messaing";

export class ContentScriptMessagingClient implements ContentScriptMessagingClient {
    private readonly events = new EventTarget();
    private readonly port: PortClientFacade;

    private commandId = 0;

    private readonly _r = Math.random();

    private readonly handlers: Record<string, (event: CommandEvent) => void> = {};

    constructor(facade: ContentScriptClientFacade, name?: string) {
        //TODO persistent connection?
        this.port = facade.connect(name);
        this.port.onMessage(message => this.handleMessage(message));
    }

    on<TEvent extends Handler<string, (...args: any[]) => void>>(
        event: TEvent["event"], 
        fn: TEvent["fn"]
    ): void {
        this.events.addEventListener(event, (message) => {
            const payload = (message as CustomEvent<Parameters<TEvent['fn']>>).detail;
            fn(...payload);
        })
    }

    once<TEvent extends Handler<string, (...args: any[]) => void>>(
        event: TEvent["event"], 
        fn: TEvent["fn"]
    ): void {
        this.events.addEventListener(event, (message) => {
            const payload = (message as CustomEvent<Parameters<TEvent['fn']>>).detail;
            fn(...payload);
        }, { once: true })
    }

    off<TEvent extends Handler<string, (...args: any[]) => void>>(
        event: TEvent["event"], 
        fn: TEvent["fn"]
    ): void {
        this.events.removeEventListener(event, fn);
    }

    emit<TEvent extends Handler<string, (...args: any[]) => void>>(
        event: TEvent["event"], 
        ...args: Parameters<TEvent["fn"]>
    ): void {
        this.port.postMessage({ type: "event", event, payload: args } as MessagingEvent)
    }

    invoke<TEvent extends Handler<string, (...args: any[]) => any>>(
        event: TEvent["event"], 
        ...args: Parameters<TEvent["fn"]>
    ): ReturnType<TEvent["fn"]> extends Promise<any> 
        ? ReturnType<TEvent["fn"]> 
        : Promise<ReturnType<TEvent["fn"]>> {
        return new Promise((resolve, reject) => {
            const payload = {
                type: "command",
                id: this.commandId++,
                event,
                payload: args
            } as CommandEvent;
            const eventName = this.getCommandEventName(payload);
            this.handlers[eventName] = (message: CommandEvent) => {
                if (message.error) {
                    reject(message.error);
                    return;
                }

                resolve(message.payload);
            };
            this.port.postMessage(payload);
        }) as any;
    }

    private handleMessage(message: object): void {
        const messagedEvent = message as MessagingEvent;

        if (messagedEvent.type !== "event" && messagedEvent.type !== "command" || !messagedEvent.event) {
            return console.warn("Unknown message type received from backgroud script. Skip", message);
        }

        if (messagedEvent.type === "command") {

            const eventName = this.getCommandEventName(messagedEvent as CommandEvent);

            const handler = this.handlers[eventName];

            if (!handler) {
                console.warn("No handler found for command", eventName, message);
                return;
            }

            handler(messagedEvent as CommandEvent);
            return;
        }

        this.events.dispatchEvent(new CustomEvent(messagedEvent.event, { detail: messagedEvent.payload }))
    }

    private getCommandEventName(event: CommandEvent): string {
        return `${event.event}:${event.id}`;
    }
}