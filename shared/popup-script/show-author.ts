import { AUTHOR_KEY } from "../constants/background";
import { SessionStorageFacade } from "../facade/storage/storage";
import { AuthorsService } from "../utils/authors-service";

type State = {
    censored: boolean;
    byUser: boolean;
    confirmation: boolean;
}

const ids = {
    popup: "popup",
    authorName: "author-name",
    authorId: "author-id",
    actionCensore: "action-censore",
    actionUncensore: "action-uncensore",
    actionUncensoreConfirm: "uncensore-confirmation-yes",
    actionUncensoreCancel: "uncensore-confirmation-no",
} as const;

type Elements = {
    - readonly [K in keyof typeof ids]: HTMLElement;
}

const getElementById = (id: string): HTMLElement => {
    const element = document.getElementById(id);
    if (!element) {
        throw new Error(`Element with id ${id} not found`);
    }
    return element;
}

const getElements = (): Elements => {
    const elements = {} as Elements;
    for (const id in ids) {
        elements[id as keyof typeof ids] = getElementById(ids[id as keyof typeof ids]);
    }
    return elements;
}

const setStateFromStatus = (state: State, status: AuthorStatus) => {
    state.censored = !status.allowed;
    state.byUser = (status.inStatic && status.allowed) || (!status.inStatic && !status.allowed);
}

export const start = async (
    sessionStorage: SessionStorageFacade,
    authorsService: AuthorsService,
) => {
    console.debug("Popup script started");

    const state: State = {
        censored: false,
        byUser: false,
        confirmation: false,
    }

    const refreshAuthorStatus = async (state: State, author: Author): Promise<void> => {
        const status = await authorsService.getAuthorStatus(author.id, author.type);
        console.debug("Author status", status);
        setStateFromStatus(state, status);
    }

    const author = await sessionStorage.get(AUTHOR_KEY);

    await authorsService.getBlockedAuthorsOnPlatform(author.type);

    await refreshAuthorStatus(state, author);

    console.debug("Author from session storage", author);

    const elements = getElements();
    const updatePopup = async () => {
        if (state.censored) {
            elements.popup.classList.remove("uncensored");
            elements.popup.classList.add("censored");
        } else {
            elements.popup.classList.remove("censored");
            elements.popup.classList.add("uncensored");
        }

        if (state.byUser) {
            elements.popup.classList.add("by-user");
        } else {
            elements.popup.classList.remove("by-user");
        }

        if (state.censored && state.confirmation) {
            elements.actionUncensore.classList.add("confirm");
        } else {
            elements.actionUncensore.classList.remove("confirm");
        }
    }

    const censore = async () => {
        console.debug("Censore author", author);

        await authorsService.blockAuthor(author);
        await refreshAuthorStatus(state, author);
        updatePopup();
    }

    const uncensoreStart = () => {
        console.debug("Uncensore author", author);

        state.confirmation = true;
        updatePopup();
    }

    const uncensoreCancel = () => {
        console.debug("Uncensor canceled", author);

        state.confirmation = false;
        updatePopup();
    }

    const uncensore = async () => {
        console.debug("Uncensore confirmed", author);

        await authorsService.allowAuthor(author);
        await refreshAuthorStatus(state, author);
        state.confirmation = false;
        updatePopup();
    }

    elements.authorName.innerText = author.name;
    elements.authorId.innerText = author.id;

    elements.actionCensore.addEventListener("click", censore);
    elements.actionUncensore.addEventListener("click", uncensoreStart);
    elements.actionUncensoreConfirm.addEventListener("click", uncensore);
    elements.actionUncensoreCancel.addEventListener("click", uncensoreCancel);

    updatePopup();
}