import { LocalAuthorsList } from "./local-authors-list";
import { StaticAuthorsList } from "./static-authors-list";

export class AuthorsService {
    private readonly staticAuthors: Authors;

    constructor(
        private readonly localList: LocalAuthorsList,
        private readonly staticList: StaticAuthorsList,
    ) {
        this.staticAuthors = staticList.getStaticAuthors();
    }

    public async getAuthorStatus(id: string, type: Platform): Promise<AuthorStatus> {
        const inStatic = this.staticList.inStaticAuthorsList(id, type);
        const allowed = inStatic 
            ? await this.localList.inAllowlist(id, type)
            : !(await this.localList.inBlocklist(id, type));

        return { inStatic, allowed };
    }

    public async getBlockedAuthorsOnPlatform(type: Platform): Promise<Author[]> {
        const inAllowlist = (await this.localList.getAllowlist())[type];
        const inBlocklist = (await this.localList.getBlocklist())[type];

        return this.staticAuthors[type]
            .filter(author => !inAllowlist.find(a => a.id === author.id))
            .concat(inBlocklist);
    }

    public async allowAuthor(author: Author): Promise<void> {
        await this.localList.removeFromBlocklist(author);

        if (this.staticAuthors[author.type]?.find(a => a.id === author.id)) {
            return await this.localList.addToAllowlist(author);
        }
    }

    public async blockAuthor(author: Author): Promise<void> {
        await this.localList.removeFromAllowlist(author);

        if (!this.staticAuthors[author.type]?.find(a => a.id === author.id)) {
            return await this.localList.addToBlocklist(author);
        }
    }
}