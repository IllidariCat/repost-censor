import vkAuthors from "../config/vk.json";
import twitterAuthors from "../config/twitter.json";

const fileToAuthorsArray = (file: Record<string, string>): Author[] => 
    Object.entries(file).map(([id, name]) => ({ id, name, type: "twitter" }));

export class StaticAuthorsList {
    private readonly staticAuthors: Authors = {
        twitter: fileToAuthorsArray(twitterAuthors),
        vk: fileToAuthorsArray(vkAuthors),
    }

    public inStaticAuthorsList(id: string, type: Platform): boolean {
        return !!this.staticAuthors[type]?.find(a => a.id === id);
    }

    public getStaticAuthors(): Authors {
        const authors: Authors = {
            twitter: fileToAuthorsArray(twitterAuthors),
            vk: fileToAuthorsArray(vkAuthors),
        };

        return authors;
    }
}