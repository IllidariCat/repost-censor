import { Platform } from "../constants/platforms";
import { SyncStorageFacade } from "../facade/storage/storage";

export const blocklistKey = "rc-blocklist";
export const allowlistKey = "rc-allowlist";

export class LocalAuthorsList {
    constructor(private readonly syncStorage: SyncStorageFacade) {}

    public async getAllowlist(): Promise<Authors> {
        return (await this.syncStorage.get(allowlistKey)) || this.getDefault();
    }

    public async getBlocklist(): Promise<Authors> {
        return (await this.syncStorage.get(blocklistKey)) || this.getDefault();
    }

    public async inAllowlist(id: string, type: Platform): Promise<boolean> {
        const authors = await this.getAllowlist();
        return !!authors[type]?.find(a => a.id === id);
    }

    public async inBlocklist(id: string, type: Platform): Promise<boolean> {
        const authors = await this.getBlocklist();
        return !!authors[type]?.find(a => a.id === id);
    }

    public async addToBlocklist(author: Author): Promise<void> {
        const authors = await this.getBlocklist();

        let existingAuthor;
        if (existingAuthor = authors[author.type]?.find(a => a.id === author.id)) {
            existingAuthor.name = author.name;
        } else if (authors[author.type]) {
            authors[author.type].push(author);
        } else {
            console.warn("Unknown author type", author);
            return;
        }

        await this.syncStorage.set(blocklistKey, authors);
    }

    public async addToAllowlist(author: Author): Promise<void> {
        const authors = await this.getAllowlist();

        let existingAuthor;
        if (existingAuthor = authors[author.type]?.find(a => a.id === author.id)) {
            existingAuthor.name = author.name;
        } else if (authors[author.type]) {
            authors[author.type].push(author);
        } else {
            console.warn("Unknown author type", author);
            return;
        }

        await this.syncStorage.set(allowlistKey, authors);
    }

    public async removeFromBlocklist(author: Author): Promise<void> {
        const authors = await this.getBlocklist();

        if (!authors[author.type]) {
            console.warn("Unknown author type", author);
            return;
        }

        const index = authors[author.type].findIndex(a => a.id === author.id);
        if (index === -1) {
            return;
        }

        authors[author.type].splice(index, 1);
        await this.syncStorage.set(blocklistKey, authors);
    }

    public async removeFromAllowlist(author: Author): Promise<void> {
        const authors = await this.getAllowlist();

        if (!authors[author.type]) {
            console.warn("Unknown author type", author);
            return;
        }

        const index = authors[author.type].findIndex(a => a.id === author.id);
        if (index === -1) {
            return;
        }

        authors[author.type].splice(index, 1);
        await this.syncStorage.set(allowlistKey, authors);
    }

    public async clearFromStatic(staticAuthors: Authors): Promise<void> {
        const allowlist = await this.getAllowlist();
        const blocklist = await this.getBlocklist();

        for (let platform of Object.keys(Platform)) {
            const key = platform as Platform;
            allowlist[key] = allowlist[key].filter(a => staticAuthors[key].find(s => s.id === a.id));
            blocklist[key] = blocklist[key].filter(a => !staticAuthors[key].find(s => s.id === a.id));
        }

        await this.syncStorage.set(allowlistKey, allowlist);
        await this.syncStorage.set(blocklistKey, blocklist);
    }

    private getDefault(): Authors {
        return {
            twitter: [],
            vk: [],
        }
    }
}