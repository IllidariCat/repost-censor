import { BackgroundScriptClientFacade, ContentScriptClientFacade, PortClientFacade } from "./messaging";

export class ChromeBackgroundScriptClient implements BackgroundScriptClientFacade {
    onConnect(handler: (port: PortClientFacade) => void): void {
        chrome.runtime.onConnect.addListener(port => handler(new ChromePortClient(port)));
    }
}

export class ChromeContentScriptClient implements ContentScriptClientFacade {
    connect(name?: string | undefined): PortClientFacade {
        const port = chrome.runtime.connect({ name });
        return new ChromePortClient(port);
    }
}

export class ChromePortClient implements PortClientFacade {
    constructor(private readonly port: chrome.runtime.Port) {}

    get tabId(): number | undefined {
        return this.port.sender?.tab?.id;
    }

    onMessage(handler: (message: any) => void): void {
        this.port.onMessage.addListener(handler);
    }

    postMessage(message: any): void {
        this.port.postMessage(message);
    }

    onDisconnect(handler: () => void): void {
        this.port.onDisconnect.addListener(handler);
    }
}