import { BackgroundScriptClientFacade, ContentScriptClientFacade, PortClientFacade } from "./messaging";

export class FirefoxBackgroundScriptClient implements BackgroundScriptClientFacade {
    onConnect(handler: (port: PortClientFacade) => void): void {
        browser.runtime.onConnect.addListener(port => handler(new FirefoxPortClient(port)));
    }
}

export class FirefoxContentScriptClient implements ContentScriptClientFacade {
    connect(name?: string | undefined): PortClientFacade {
        const port = browser.runtime.connect({ name });
        return new FirefoxPortClient(port);
    }
}

export class FirefoxPortClient implements PortClientFacade {
    constructor(private readonly port: browser.runtime.Port) {}

    get tabId(): number | undefined {
        return this.port.sender?.tab?.id;
    }

    onMessage(handler: (message: any) => void): void {
        this.port.onMessage.addListener(handler);
    }

    postMessage(message: any): void {
        this.port.postMessage(message);
    }

    onDisconnect(handler: () => void): void {
        this.port.onDisconnect.addListener(handler);
    }
}