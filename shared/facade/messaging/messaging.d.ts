export interface BackgroundScriptClientFacade {
    onConnect(handler: (port: PortClientFacade) => void): void;
}

export interface ContentScriptClientFacade {
    connect(name?: string): PortClientFacade;
}

export interface PortClientFacade {
    tabId: number | undefined;
    onMessage(handler: (message: any) => void): void;
    postMessage(message: any): void;
    onDisconnect(handler: () => void): void;
}