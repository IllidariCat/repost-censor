import { PageActionFacade } from "./page-action";

export class ChromePageActionService implements PageActionFacade {
    hide(tabId: number): Promise<void> {
        return new Promise((resolve) => {
            chrome.pageAction.hide(tabId, () => {
                resolve();
            });
        });
    }

    show(tabId: number): Promise<void> {
        return new Promise((resolve) => {
            chrome.pageAction.show(tabId, () => {
                resolve();
            });
        });
    }
}