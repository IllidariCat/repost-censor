import { PageActionFacade } from "./page-action";

export class FirefoxPageActionService implements PageActionFacade {
    hide(tabId: number): Promise<void> {
        return browser.pageAction.hide(tabId);
    }

    show(tabId: number): Promise<void> {
        return browser.pageAction.show(tabId);
    }
}