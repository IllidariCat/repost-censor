export interface PageActionFacade {
    hide(tabId: number): Promise<void>;
    show(tabId: number): Promise<void>;
}