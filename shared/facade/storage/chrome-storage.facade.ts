import { SyncStorageFacade, SessionStorageFacade } from "./storage";

export class ChromeSyncStorage implements SyncStorageFacade {
    private readonly storage: chrome.storage.StorageArea;
    constructor() {
        this.storage = chrome.storage.sync;
    }
    async get(key: string) {
        return new Promise<any>((resolve, reject) => {
            this.storage.get(key, result => {
                if (chrome.runtime.lastError) {
                    reject(chrome.runtime.lastError);
                } else {
                    resolve(result[key]);
                }
            });
        });
    }
    async set(key: string, value: any) {
        return new Promise<void>((resolve, reject) => {
            this.storage.set({ [key]: value }, () => {
                if (chrome.runtime.lastError) {
                    reject(chrome.runtime.lastError);
                } else {
                    resolve();
                }
            });
        });
    }
    async remove(key: string) {
        return new Promise<void>((resolve, reject) => {
            this.storage.remove(key, () => {
                if (chrome.runtime.lastError) {
                    reject(chrome.runtime.lastError);
                } else {
                    resolve();
                }
            });
        });
    }
    async clear() {
        return new Promise<void>((resolve, reject) => {
            this.storage.clear(() => {
                if (chrome.runtime.lastError) {
                    reject(chrome.runtime.lastError);
                } else {
                    resolve();
                }
            });
        });
    }
}

export class ChromeSessionStorage implements SessionStorageFacade { 
    private readonly storage: chrome.storage.StorageArea;
    constructor() {
        this.storage = chrome.storage.session;
    }
    async get(key: string) {
        return new Promise<any>((resolve, reject) => {
            this.storage.get(key, result => {
                if (chrome.runtime.lastError) {
                    reject(chrome.runtime.lastError);
                } else {
                    resolve(result[key]);
                }
            });
        });
    }
    async set(key: string, value: any) {
        return new Promise<void>((resolve, reject) => {
            this.storage.set({ [key]: value }, () => {
                if (chrome.runtime.lastError) {
                    reject(chrome.runtime.lastError);
                } else {
                    resolve();
                }
            });
        });
    }
    async remove(key: string) {
        return new Promise<void>((resolve, reject) => {
            this.storage.remove(key, () => {
                if (chrome.runtime.lastError) {
                    reject(chrome.runtime.lastError);
                } else {
                    resolve();
                }
            });
        });
    }
    async clear() {
        return new Promise<void>((resolve, reject) => {
            this.storage.clear(() => {
                if (chrome.runtime.lastError) {
                    reject(chrome.runtime.lastError);
                } else {
                    resolve();
                }
            });
        });
    }
}