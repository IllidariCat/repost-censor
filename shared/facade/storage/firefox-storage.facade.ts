import { SyncStorageFacade, SessionStorageFacade } from "./storage";

export class FirefoxSyncStorage implements SyncStorageFacade {
    private readonly storage: browser.storage.StorageArea;
    constructor() {
        this.storage = browser.storage.sync;
    }
    async get(key: string) {
        const result = await this.storage.get(key);
        return result[key];
    }
    async set(key: string, value: any) {
        await this.storage.set({ [key]: value });
    }
    async remove(key:string ) {
        await this.storage.remove(key);
    }
    async clear() {
        await this.storage.clear();
    }
}

export class FirefoxSessionStorage implements SessionStorageFacade {
    private readonly storage: browser.storage.StorageArea;
    constructor() {
        this.storage = browser.storage.session;
    }
    async get(key: string) {
        const result = await this.storage.get(key);
        return result[key];
    }
    async set(key: string, value: any) {
        await this.storage.set({ [key]: value });
    }
    async remove(key:string ) {
        await this.storage.remove(key);
    }
    async clear() {
        await this.storage.clear();
    }
}