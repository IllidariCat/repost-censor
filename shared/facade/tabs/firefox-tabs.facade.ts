import { TabsFacade } from "./tabs";

export class FirefoxTabs implements TabsFacade {
    onActivated(callback: (tabId: number) => void): void {
        browser.tabs.onActivated.addListener((activeInfo) => {
            callback(activeInfo.tabId);
        });
    }
}