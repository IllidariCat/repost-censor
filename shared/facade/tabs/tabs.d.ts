export interface TabsFacade {
    onActivated: (callback: (tabId: number) => void) => void;
}