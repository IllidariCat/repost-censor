import { TabsFacade } from "./tabs";

export class ChromeTabs implements TabsFacade {
    onActivated(callback: (tabId: number) => void): void {
        chrome.tabs.onActivated.addListener((activeInfo) => {
            callback(activeInfo.tabId);
        });
    }
}